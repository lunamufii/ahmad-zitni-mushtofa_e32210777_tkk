import 'package:flutter/material.dart';
import 'package:dashed_container/dashed_container.dart';

class RowColumnPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.dashboard),
          title: Text("Styling App | Row & Column"),
          actions: <Widget>[
            Icon(Icons.search),
            // Icon(Icons.find_in_page)
          ],
          actionsIconTheme: IconThemeData(color: Colors.yellow),
          backgroundColor: Colors.teal,
          bottom: PreferredSize(
              child: Container(
                color: Colors.amber[100],
                height: 4.0,
              ),
              preferredSize: Size.fromHeight(4.0)),
          centerTitle: true,
        ),
        //PERUBAHAN BARU
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.teal.shade900,
          child: Text('+'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 100),
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.teal[600], shape: BoxShape.circle),
            ),
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.blue[600], shape: BoxShape.rectangle),
            ),
            Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.teal[600], shape: BoxShape.circle),
            ),
            Row(
              //TAMBAHKAN CODE INI
              mainAxisAlignment: MainAxisAlignment.center,
              //TAMBAHKAN CODE INI
              children: <Widget>[
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.teal[600], shape: BoxShape.circle),
                ),
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.blue[600], shape: BoxShape.rectangle),
                ),
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.teal[600], shape: BoxShape.circle),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20.0, right: 20.0),
                ),
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.teal[600], shape: BoxShape.circle),
                ),
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.blue[600], shape: BoxShape.rectangle),
                ),
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.teal[600], shape: BoxShape.circle),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.teal[600],
                      shape: BoxShape.circle,
                    ),
                ),
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.blue[600], shape: BoxShape.rectangle),
                ),
                Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.teal[600], shape: BoxShape.circle),
                ),
                SizedBox(height: 35),
                DashedContainer(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: FlutterLogo(
                        size: 15,
                      ),
                    ),
                    dashColor: Colors.black,
                    boxShape: BoxShape.circle,
                    dashedLength: 30.0,
                    blankLength: 6.0,
                    strokeWidth: 6.0,
                ),
                SizedBox(height: 35),
                SizedBox(
                  width: 250.0,
                  height: 60.0,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal.shade700,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/about');
                    },
                    child: Text('Kembali ke Halaman Utama'),
                  ),
                ),
              ],
            ),
          ],
        ),
        //PERUBAHAN BARU
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
