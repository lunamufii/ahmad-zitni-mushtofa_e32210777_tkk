import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ahmad Zitni Mushtofa | E32210777"),
        backgroundColor: Colors.teal,
        bottom: PreferredSize(
            child: Container(
              color: Colors.amber[100],
              height: 4.0,
            ),
            preferredSize: Size.fromHeight(4.0)
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.teal.shade700,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/about');
              },
              child: Text('Tap Untuk ke About Page'),
            ),
            SizedBox(height: 10),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.teal.shade700,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/halaman-404');
              },
              child: Text('Tap Halaman lain'),
            ),
            SizedBox(height: 150),
            SizedBox(
              width: 250.0,
              height: 60.0,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.teal.shade700,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/row');
                },
                child: Text('Tap Untuk ke Row & Column'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tentang Aplikasi'),
        actionsIconTheme: IconThemeData(color: Colors.yellow),
        backgroundColor: Colors.teal,
        bottom: PreferredSize(
            child: Container(
              color: Colors.amber[100],
              height: 4.0,
            ),
            preferredSize: Size.fromHeight(4.0)),
        centerTitle: true,
      ),
      body: Center(
        child: SizedBox(
          width: 250.0,
          height: 60.0,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              // shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              //   RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0),
              //   side: BorderSide(color: Colors.teal),
              // ),
              primary: Colors.teal.shade700,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/halaman-404');
            },
            child: Text('Kembali ke Halaman Utama'),
          ),
        ),
      ),
    );
  }
}
