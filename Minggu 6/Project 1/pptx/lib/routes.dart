import 'package:flutter/material.dart';
import 'package:pptx/rowColumn.dart';
import 'package:pptx/screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // jika ingin mengirim argument
    // final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => HomePage());
      case '/about':
        return MaterialPageRoute(builder: (_) => AboutPage());
      // return MaterialPageRoute(builder: (_) => AboutPage(args));
      case '/row':
        return MaterialPageRoute(builder: (_) => RowColumnPage());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
          actionsIconTheme: IconThemeData(color: Colors.yellow),
          backgroundColor: Colors.red[800],
          bottom: PreferredSize(
              child: Container(
                color: Colors.amber[100],
                height: 4.0,
              ),
              preferredSize: Size.fromHeight(4.0)),
          centerTitle: true,
        ),
        body: Container(
          alignment: Alignment.center,
          child: SizedBox.fromSize(
          size: Size(100, 56),
          child: Container(
            child: Material( // button color
              child: InkWell(
                splashColor: Colors.green, // splash color
                onTap: () {}, // button pressed
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.warning), // icon
                    Text("404 Not Found"), // text
                  ],
                ),
              ),
            ),
          ),
        ),
        ),
      );
    });
  }
}
