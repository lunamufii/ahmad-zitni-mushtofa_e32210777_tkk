class Product {
  final int id;
  final String productName;
  final String productImage;
  final String produtDescription;
  final double price;

  Product(this.id, this.productName, this.productImage, this.produtDescription,
      this.price);
}
