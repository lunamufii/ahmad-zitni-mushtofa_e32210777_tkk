import 'package:flutter/material.dart';

void main() => runApp(BelajarImage());

class BelajarImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Ahmad Zitni Mushtofa  |  E32210777"),
        ),
        body: Image.asset('assets/images/silhouette-girl.jpg'),
      ),
    );
  }
}
