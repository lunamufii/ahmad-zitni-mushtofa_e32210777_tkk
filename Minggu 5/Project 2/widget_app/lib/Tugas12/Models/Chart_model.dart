class ChartModel {
  final String? name;
  final String? message;
  final String? time;
  final String? profileUrl;

  ChartModel({this.name, this.message, this.time, this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'Airdrop Planet',
      message: 'Pengen sukses kok males-malesan dek ?',
      time: '05.00',
      profileUrl:
          'https://pbs.twimg.com/profile_images/1567946464961400832/5JfQ9JzY_400x400.jpg'),
  ChartModel(
      name: 'Politeknik Negeri Jember',
      message: 'Semangat gan ! Ojo valo ae mad',
      time: '04.15',
      profileUrl:
          'https://cdn.kibrispdr.org/data/761/logo-polije-png-3.png'),
  ChartModel(
      name: 'Irnanda',
      message: 'Valo kah dek ? Login kah ?',
      time: '00.03',
      profileUrl:
          'https://api.ivpl.co.id/uploads/team/906/rrq.png'),
  ChartModel(
      name: 'Nararga',
      message: 'Ya masak ga login valo sih bang aduhhh',
      time: '00.00',
      profileUrl:
          'https://w7.pngwing.com/pngs/283/582/png-transparent-league-of-legends-ahri-desktop-rendering-league-of-legends-flower-fictional-character-desktop-wallpaper.png'),
  ChartModel(
      name: 'Nico',
      message: 'Ayo tuku mangan, mbok deka a ?',
      time: '11 September',
      profileUrl:
          'https://i.pinimg.com/564x/ee/60/0b/ee600b5178e4f1648fd1e8623f049611.jpg'),
  ChartModel(
      name: 'Mas Wahyu Kost',
      message: 'Infokan loker senin-sabtu libur minggu gajian',
      time: '11 September',
      profileUrl:
          'https://i.pinimg.com/736x/fb/6f/99/fb6f99f760733ce2c47ee8f57668050b.jpg'),
  ChartModel(
      name: 'Daffa Airdrop',
      message: 'Sudah belum JP hari ini ???',
      time: '11 September',
      profileUrl:
          'https://p.kindpng.com/picc/s/5-51409_meme-sad-frog-clipart-sad-frog-hd-png.png'),
  ChartModel(
      name: 'Habib Jindan bin King Salman',
      message: '10 September',
      time: '12.21',
      profileUrl:
          'https://us-tuna-sounds-images.voicemod.net/c584d8eb-d074-47a5-ae60-3f69bb36323b-1661517020295.png'),
  ChartModel(
      name: 'Mas Gilang Kost',
      message: '8 September',
      time: '13.41',
      profileUrl:
          'https://miscmedia-9gag-fun.9cache.com/images/thumbnail-facebook/1557453152.4514_U2asE8_n.jpg'),
];
