class PopularMovies {
  late int page;
  late int totalResults;
  late int totalPages;
  late List<Results> results;

  PopularMovies(
    {
    required this.page,
    required this.totalResults,
    required this.totalPages,
    required this.results
    }
  );

  PopularMovies.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    totalResults = json['total_results'];
    totalPages = json['total_pages'];
    if (json['results'] != null) {
      results = <Results>[];
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['total_results'] = this.totalResults;
    data['total_pages'] = this.totalPages;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Results {
  late List<int> genreIds;
  late int voteCount;
  late int id;
  late bool video;
  late bool adult;
  late double popularity;
  late double voteAverage;
  late String posterPath;
  late String backdropPath;
  late String originalLanguage;
  late String originalTitle;
  late String title;
  late String overview;
  late String releaseDate;

  Results(
    {
      required this.genreIds,
      required this.voteCount,
      required this.id,
      required this.video,
      required this.adult,
      required this.popularity,
      required this.voteAverage,
      required this.posterPath,
      required this.backdropPath,
      required this.originalLanguage,
      required this.originalTitle,
      required this.title,
      required this.overview,
      required this.releaseDate,
    }
  );

  Results.fromJson(Map<String, dynamic> json) {
    genreIds = json['genre_ids'].cast<int>();
    voteCount = json['vote_count'];
    id = json['id'];
    video = json['video'];
    adult = json['adult'];
    popularity = json['popularity'];
    voteAverage = json['vote_average'].toDouble();
    posterPath = json['poster_path'];
    backdropPath = json['backdrop_path'];
    originalLanguage = json['original_language'];
    originalTitle = json['original_title'];
    title = json['title'];
    overview = json['overview'];
    releaseDate = json['release_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['genre_ids'] = this.genreIds;
    data['vote_count'] = this.voteCount;
    data['id'] = this.id;
    data['video'] = this.video;
    data['adult'] = this.adult;
    data['popularity'] = this.popularity;
    data['vote_average'] = this.voteAverage;
    data['poster_path'] = this.posterPath;
    data['backdrop_path'] = this.backdropPath;
    data['original_language'] = this.originalLanguage;
    data['original_title'] = this.originalTitle;
    data['title'] = this.title;
    data['overview'] = this.overview;
    data['release_date'] = this.releaseDate;
    return data;
  }
}